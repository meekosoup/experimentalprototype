﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Vestigia
{
    public enum DamageType { Universal, Blunt, Cut, Stab, Fire, Ice, Acid, Poison, Lightning, Magic }
    public enum VitalType { Blood, Bone, Metal, Ickor }
    public enum VestigeType { Unknown, Angle, Demon, Beast, Bug }

    [CreateAssetMenu(fileName = "GameData", menuName = "Game/GameData")]
    public class GameData : SerializedScriptableObject
    {
        public OrganAtlas organAtlas;
        public VestigeAtlas vestigeAtlas;
    }
}