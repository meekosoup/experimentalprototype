﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    public class DamagePacket
    {
        public float damage;
        public DamageType damageType;

        public DamagePacket(float damage = 0, DamageType damageType = DamageType.Blunt)
        {
            this.damage = damage;
            this.damageType = damageType;
        }
    }
}