﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    [System.Serializable]
    public class Vestige
    {

        private VestigeData vestigeData;
        private string displayName, internalName;
        private VestigeType vestigeType;
        private Organ body;
        private Organ head;
        private Organ arm_left;
        private Organ arm_right;
        private Organ legs;
        private List<Organ> addons;

        public VestigeData GetVestigeData()
        {
            return vestigeData;
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value; }
        }
        public string InternalName { get { return internalName; } }

        public VestigeType GetVestigeType()
        {
            return vestigeType;
        }

        public Organ Body { get { return body; } }
        public Organ Head { get { return head; } }
        public Organ Arms_Left { get { return arm_left; } }
        public Organ Arms_Right { get { return arm_right; } }
        public Organ Legs { get { return legs; } }
        public List<Organ> Extras { get { return addons; } }

        public Vestige(VestigeData vestigeData)
        {
            this.vestigeData = vestigeData;
            displayName = vestigeData.displayName;
            internalName = vestigeData.internalName;
            vestigeType = vestigeData.vestigeType;
            body = new Organ(vestigeData.body);
            head = new Organ(vestigeData.head);
            arm_left = new Organ(vestigeData.arm_left);
            arm_right = new Organ(vestigeData.arm_right);
            legs = new Organ(vestigeData.legs);

            addons = new List<Organ>();

            foreach (var extra in vestigeData.extras)
            {
                addons.Add(new Organ(extra));
            }
        }

        public float TotalHealth()
        {
            float amount = body.Health + head.Health;

            amount += arm_left.Health + arm_right.Health + legs.Health;

            foreach (var organ in addons)
                amount += organ.Health;

            return amount;
        }

        public float TotalMaxHealth()
        {
            float amount = body.MaxHealth() + head.MaxHealth();

            amount += arm_left.MaxHealth() + arm_right.MaxHealth() + legs.MaxHealth();

            foreach (var organ in addons)
                amount += organ.MaxHealth();

            return amount;
        }

        public VestigeData Clone()
        {
            return (VestigeData)this.MemberwiseClone();
        }
    }
}