﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    public class SkillAffinity
    {
        public float power, control, defense;

        public SkillAffinity Combine(SkillAffinity affinity)
        {
            SkillAffinity newAffinity = new SkillAffinity();

            newAffinity.power += affinity.power;
            newAffinity.control += affinity.control;
            newAffinity.defense += affinity.defense;

            return newAffinity;
        }

        public void Absorb(SkillAffinity affinity)
        {
            power += affinity.power;
            control += affinity.control;
            defense += affinity.defense;
        }
    }
}