﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager manager;
        public GameData data;

        private void Awake()
        {
            if (manager == null)
                manager = this;

            if (manager != this)
                Destroy(gameObject);
        }
    }
}