﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    public class Vitals
    {
        public float blood, bone, ichor, wood, metal, plastic;

        /// <summary>
        /// Used for repair amounts.
        /// </summary>
        /// <param name="factor">percentage to apply to each vital.</param>
        /// <returns></returns>
        public Vitals Factor(float factor)
        {
            Vitals vitals = new Vitals();

            vitals.blood = blood * factor;
            vitals.bone = bone * factor;
            vitals.ichor = ichor * factor;
            vitals.wood = wood * factor;
            vitals.metal = metal * factor;
            vitals.plastic = plastic * factor;

            return vitals;
        }

        public void Absorb(Vitals vitals)
        {
            blood = vitals.blood;
            bone = vitals.bone;
            ichor = vitals.ichor;
            wood = vitals.wood;
            metal = vitals.metal;
            plastic = vitals.plastic;
        }
    }
}