﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Vestigia
{
    [CreateAssetMenu(fileName = "OrganAtlas", menuName = "Vestige/OrganAtlas")]
    public class OrganAtlas : SerializedScriptableObject
    {
        public static string BODY_GHOUL = "body_ghoul";
        public static string HEAD_GHOUL = "head_ghoul";
        public static string ARM_GHOUL = "arm_ghoul";
        public static string LEG_GHOUL = "leg_ghoul";

        public Dictionary<string, OrganData> bodies;
        public Dictionary<string, OrganData> heads;
        public Dictionary<string, OrganData> arms;
        public Dictionary<string, OrganData> legs;
        public Dictionary<string, OrganData> skins;
        public Dictionary<string, OrganData> extras;

        public OrganAtlas()
        {
            bodies = new Dictionary<string, OrganData>();
            heads = new Dictionary<string, OrganData>();
            arms = new Dictionary<string, OrganData>();
            legs = new Dictionary<string, OrganData>();
            skins = new Dictionary<string, OrganData>();
            extras = new Dictionary<string, OrganData>();
        }
    }
}