﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    [System.Serializable]
    public class Organ
    {
        private OrganData organData;
        private string displayName, internalName;
        private float mass, health, healthModifier;
        private OrganType organType;
        private VestigeType vestigeType;
        private VestigeSpecies vestigeSpecies;
        private Vitals vitals;
        private ResistancesData resistances;
        private Skills skills;

        public OrganData GetOrganDate()
        {
            return organData;
        }

        public string DisplayName { get { return displayName; } }
        public string InternalName { get { return internalName; } }
        public float Mass { get { return mass; } }
        public float Health { get { return health; } }
        public float HealthModifier { get { return healthModifier; } }

        public OrganType GetOrganType()
        {
            return organType;
        }

        public VestigeType GetVestigeType()
        {
            return vestigeType;
        }

        public VestigeSpecies GetVestigeSpecies()
        {
            return vestigeSpecies;
        }

        public Vitals GetVitals()
        {
            return vitals;
        }

        public ResistancesData GetResistances()
        {
            return resistances;
        }

        public Skills GetSkills()
        {
            return skills;
        }

        public Organ(OrganData organData)
        {
            this.organData = organData;
            displayName = organData.displayName;
            internalName = organData.internalName;
            mass = organData.mass;
            healthModifier = organData.healthModifier;
            organType = organData.organType;
            vestigeType = organData.vestigeType;
            vestigeSpecies = organData.vestigeSpecies;
            vitals = organData.vitals;
            resistances = organData.resistances;

            skills = organData.skills;

            health = MaxHealth();
        }

        public float GetMass()
        {

            return mass;
        }

        public float MaxHealth()
        {
            return GetMass() * healthModifier;
        }

        public float GetHealthFactor()
        {
            return health / MaxHealth();
        }

        public void ResetHealth()
        {
            health = MaxHealth();
        }

        public void Hurt(float amount)
        {
            health = Mathf.Max(health - Mathf.Abs(amount), 0);
        }

        public void Heal(float amount)
        {
            health = Mathf.Max(health + Mathf.Abs(amount), MaxHealth());
        }
    }
}