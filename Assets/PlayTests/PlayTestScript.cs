﻿using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Tests
{
    public class PlayTestScript
    {
        // A Test behaves as an ordinary method
        [Test]
        public void PlayTestScriptsSimplePasses()
        {
            // Use the Assert class to test conditions
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator PlayTestScriptsWithEnumeratorPasses()
        {
            
            yield return null;
        }
    }
}
