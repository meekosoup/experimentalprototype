﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    [CreateAssetMenu(fileName = "Resistances", menuName = "Vestige/Resistances")]
    public class ResistancesData : SerializedScriptableObject
    {
        public Dictionary<DamageType, float> resistances = new Dictionary<DamageType, float>
    {
        {DamageType.Blunt, 1f},
        {DamageType.Cut, 1f},
        {DamageType.Stab, 1f},
        {DamageType.Fire, 1f},
        {DamageType.Ice, 1f},
        {DamageType.Poison, 1f},
        {DamageType.Acid, 1f},
        {DamageType.Lightning, 1f},
        {DamageType.Magic, 1f},
        {DamageType.Universal, 1f}
    };

        public ResistancesData Clone()
        {
            return this.MemberwiseClone() as ResistancesData;
        }
    }
}