﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    public class Skills
    {
        // Combat
        public SkillAffinity melee, ranged;

        public Skills()
        {
            melee = new SkillAffinity();
            ranged = new SkillAffinity();
        }

        public Skills Combine(Skills skills)
        {
            Skills newSkills = new Skills();

            newSkills.melee = melee.Combine(skills.melee);
            newSkills.ranged = ranged.Combine(skills.ranged);

            return newSkills;
        }

        public void Absorb(Skills skills)
        {
            melee.Absorb(skills.melee);
            ranged.Absorb(skills.ranged);
        }
    }
}