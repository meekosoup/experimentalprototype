﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    [CreateAssetMenu(fileName = "VestigeSpecies", menuName = "Vestige/VestigeSpecies")]
    public class VestigeSpecies : SerializedScriptableObject
    {
        private const string DEFAULT_SPECIES_NAME = "Unknown Species";

        public string displayName = DEFAULT_SPECIES_NAME;
    }
}