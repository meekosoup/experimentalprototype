﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    [CreateAssetMenu(fileName = "VestigeAtlas", menuName = "Vestige/VestigeAtlas")]
    public class VestigeAtlas : SerializedScriptableObject
    {
        public Dictionary<string, VestigeData> vestiges;

        public VestigeAtlas()
        {
            vestiges = new Dictionary<string, VestigeData>();
        }
    }
}