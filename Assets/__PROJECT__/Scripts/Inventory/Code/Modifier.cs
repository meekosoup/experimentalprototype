﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    public class Modifier
    {
        public float flat, scale;

        public float Calculate(float amount)
        {
            return (amount * scale) + flat;
        }
    }
}