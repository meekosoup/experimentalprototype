﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    [CreateAssetMenu(fileName = "Vestige", menuName = "Vestige/Vestige")]
    public class VestigeData : SerializedScriptableObject
    {
        private const string DEFAULT_VESTIGE_NAME = "Unknown Monster";
        private const string DEFAULT_INTERNAL_VESTIGE_NAME = "vestige_unknown";

        public string displayName;
        public string internalName;
        public VestigeType vestigeType = VestigeType.Unknown;
        public OrganData body;
        public OrganData head;
        public OrganData arm_left;
        public OrganData arm_right;
        public OrganData legs;
        public List<OrganData> extras;

        public Skills GetSkills()
        {
            Skills skills = new Skills();

            skills.Absorb(body.skills);
            skills.Absorb(head.skills);
            skills.Absorb(arm_left.skills);
            skills.Absorb(arm_right.skills);
            skills.Absorb(legs.skills);

            foreach (var extra in extras)
                skills.Absorb(extra.skills);

            return skills;
        }

        public float TotalHealth()
        {
            float amount = body.health + head.health;

            amount += arm_left.health + arm_right.health + legs.health;

            foreach (var organ in extras)
                amount += organ.health;

            return amount;
        }

        public float TotalMaxHealth()
        {
            float amount = body.MaxHealth() + head.MaxHealth();

            amount += arm_left.MaxHealth() + arm_right.MaxHealth() + legs.MaxHealth();

            foreach (var organ in extras)
                amount += organ.MaxHealth();

            return amount;
        }

        public VestigeData Clone()
        {
            return (VestigeData)this.MemberwiseClone();
        }

        public void Absorb(VestigeData vestige)
        {
            displayName = vestige.displayName;
            internalName = vestige.internalName;
            vestigeType = vestige.vestigeType;
            body = vestige.body;
            head = vestige.head;
            arm_left = vestige.arm_left;
            arm_right = vestige.arm_right;
            legs = vestige.legs;
            extras = vestige.extras;
        }
    }
}