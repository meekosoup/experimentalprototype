﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    public enum OrganType { Extra, Body, Head, Arm, Leg, Skin }

    [CreateAssetMenu(fileName = "Organ", menuName = "Vestige/Organ")]
    public class OrganData : SerializedScriptableObject
    {
        protected const string DEFAULT_ORGAN_NAME = "Unknown Organ";
        protected const string DEFAULT_INTERNAL_ORGAN_NAME = "organ_unknown";

        [BoxGroup("Organ")]
        public string displayName;
        [BoxGroup("Organ")]
        public string internalName;
        [BoxGroup("Organ"), AssetList]
        public VestigeSpecies vestigeSpecies;
        [BoxGroup("Organ")]
        public float mass = 1, health = 1, healthModifier = 1;
        [BoxGroup("Organ")]
        public OrganType organType;
        [BoxGroup("Organ")]
        public VestigeType vestigeType;
        [BoxGroup("Organ")]
        public Vitals vitals;
        [BoxGroup("Organ")]
        public ResistancesData resistances;
        [BoxGroup("Organ")]
        public Skills skills;

        public float MaxHealth()
        {
            return GetMass() * healthModifier;
        }

        public float GetHealthFactor()
        {
            return health / MaxHealth();
        }

        public virtual float GetMass()
        {
            return mass;
        }

        [Button]
        public void ResetHealth()
        {
            health = MaxHealth();
        }

        public OrganData Clone()
        {
            return (OrganData)MemberwiseClone();
        }
    }
}