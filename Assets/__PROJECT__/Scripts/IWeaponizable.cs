﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Vestigia
{
    public interface IWeaponizable
    {
        DamageType GetDamageType();
        float GetDamage();
        DamagePacket GetDamagePacket();
    }
}