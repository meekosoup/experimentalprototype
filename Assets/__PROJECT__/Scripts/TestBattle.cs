﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Vestigia
{
    public class TestBattle : MonoBehaviour
    {
        public TMP_Text textArea1;
        public TMP_Text textArea2;
        public VestigeData vestigeDataPlayer1;
        public VestigeData vestigeDataPlayer2;

        public Vestige vestige1;
        public Vestige vestige2;

        [BoxGroup("Vestige Body 1")]
        public Image headImage, bodyImage, leftArmImage, rightArmImage, leftLegImage, rightLegImage;

        private void Awake()
        {
            vestige1 = new Vestige(vestigeDataPlayer1);
            vestige2 = new Vestige(vestigeDataPlayer2);


            vestige1.Body.Hurt(10);
            vestige1.DisplayName = "Gargamel";
            textArea1.text = $"{vestige1.DisplayName} {vestige1.TotalHealth()} / {vestige1.TotalMaxHealth()}";

            //vestige2.Body.Hurt(10);
            vestige2.DisplayName = "Mael";
            textArea2.text = $"{vestige2.DisplayName} {vestige2.TotalHealth()} / {vestige2.TotalMaxHealth()}";
        }

        private void Update()
        {
            headImage.color = DangerColor(vestige1.Head.GetHealthFactor());
            bodyImage.color = DangerColor(vestige1.Body.GetHealthFactor());
            leftArmImage.color = DangerColor(vestige1.Arms_Left.GetHealthFactor());
            rightArmImage.color = DangerColor(vestige1.Arms_Right.GetHealthFactor());
            leftLegImage.color = DangerColor(vestige1.Legs.GetHealthFactor());
            rightLegImage.color = DangerColor(vestige1.Legs.GetHealthFactor());


            textArea1.text = $"{vestige1.DisplayName} {vestige1.TotalHealth()} / {vestige1.TotalMaxHealth()}\n{vestige1.Body.GetHealthFactor()}";
            textArea2.text = $"{vestige2.DisplayName} {vestige2.TotalHealth()} / {vestige2.TotalMaxHealth()}\n{vestige2.Body.GetHealthFactor()}";
        }

        public void DamageVestige1(float amount = 10)
        {
            //float HEAD_CHANCE = 0.1f;
            //float BODY_CHANCE = 0.5f;
            //float ARM_CHANCE = 0.2f;
            //float LEG_CHANCE = 0.2f;


            List<Organ> stillGood = new List<Organ>();
            if (vestige1.Head.Health > 0)
                stillGood.Add(vestige1.Head);
            if (vestige1.Body.Health > 0)
                stillGood.Add(vestige1.Body);
            if (vestige1.Arms_Left.Health > 0)
                stillGood.Add(vestige1.Arms_Left);
            if (vestige1.Arms_Right.Health > 0)
                stillGood.Add(vestige1.Arms_Right);
            if (vestige1.Legs.Health > 0)
                stillGood.Add(vestige1.Legs);

            if (stillGood.Count <= 0)
                return;

            int r = Random.Range(0, stillGood.Count);

            stillGood[r].Hurt(amount);

            foreach (var organ in stillGood)
            {
                Debug.Log($"{organ.GetOrganType()} = {organ.Health}");
            }
        }

        private Color DangerColor(float factor)
        {
            return new Color(1f, factor, factor);
        }

        //public void AttackVestige(Vestige vestige, )
    }
}